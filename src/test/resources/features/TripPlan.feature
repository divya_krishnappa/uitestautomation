@Tripplan
Feature: Planning a trip 
 
  @smoke
  Scenario: Planning a trip between 2 stations using Trip Planner
    Given I go to Trip Planner web site 
    When I enter From Location as "North Sydney Station"
    And I enter To Location as "Town Hall Station"
    Then I should be able to view list of trips 

