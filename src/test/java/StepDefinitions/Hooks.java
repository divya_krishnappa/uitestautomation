package StepDefinitions;

import java.util.concurrent.TimeUnit;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Hooks {
	public static WebDriver driver;
	
	@Before
	public void setUp()
	{	
		String browser=System.getProperty("Browser","chrome");
		String operatingSystem=System.getProperty("OperatingSystem","windows");
		
		if(operatingSystem.equalsIgnoreCase("windows")) {
			if(browser.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"//drivers//chromedriver.exe");
			driver=new ChromeDriver();
			} else if(browser.equalsIgnoreCase("firefox")) {
				 System.setProperty("webdriver.gecko.driver",System.getProperty("user.dir") + "//drivers//geckodriver.exe" );
	            driver = new FirefoxDriver();
			}
		}
		else if(operatingSystem.equalsIgnoreCase("mac")) {
		if(browser.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"//drivers//chromedriver");
		    driver=new ChromeDriver();
		} else if(browser.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver",System.getProperty("user.dir") + "//drivers//geckodriver" );
            driver = new FirefoxDriver();
			}
		}
		
		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
		driver.manage().window().maximize();
	}
	
	@After
	public void tearDown() {
		driver.close();
	}
}
