package StepDefinitions;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import PageObjects.HomePage;

public class TransportNSWSteps {
    WebDriver driver;
	HomePage homePage;
	
	public TransportNSWSteps() {
		driver=Hooks.driver;
	}
	
  @Given("I go to Trip Planner web site")
   public void i_go_to_trip_planner_web_site() throws IOException {

	InputStream input = new FileInputStream(System.getProperty("user.dir")+"/src/test/resources/config/config.properties");
	Properties properties= new Properties();
	properties.load(input);
	driver.get((String) properties.get("HomePageURL"));
	Assert.assertTrue("Verify Title", driver.getTitle().equalsIgnoreCase("Trip Planner | transportnsw.info"));
  }

	@When("^I enter From Location as \"([^\"]*)\"$")
	public void i_enter_From_Location_as(String fromLocation)  {
		homePage=new HomePage(driver);
		homePage.setFromTripLocation(fromLocation);
	}

	@When("^I enter To Location as \"([^\"]*)\"$")
	public void i_enter_To_Location_as(String toLocation)  {
		homePage.setToTripLocation(toLocation);
	}

	@Then("I should be able to view list of trips")
	public void i_should_be_able_to_view_list_of_trips() {
		Assert.assertTrue(driver.findElement(By.xpath("//span[text()='Public transport results']")).isDisplayed());
		Assert.assertTrue(driver.findElement(By.className("leaving-time")).isDisplayed());
		Assert.assertTrue(driver.findElement(By.className("timeline")).isDisplayed());
		Assert.assertTrue(driver.findElement(By.className("mode-transport")).isDisplayed());
		Assert.assertTrue(driver.findElement(By.className("amount")).isDisplayed());

	}

}
