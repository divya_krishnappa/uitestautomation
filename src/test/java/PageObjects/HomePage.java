package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage {
	
	public WebDriver driver;
	
	public HomePage(WebDriver driver) {
		this.driver=driver;
	}

	public void setFromTripLocation(String fromLocation) {
		driver.findElement(By.id("tniFromTripLocation")).sendKeys(fromLocation);
		new WebDriverWait(driver,10).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[text()='"+fromLocation+"']"))).click();
	}
	
	public void setToTripLocation(String toLocation) {
		driver.findElement(By.id("tniToTripLocation")).sendKeys(toLocation);
		new WebDriverWait(driver,10).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[text()='"+toLocation+"']"))).click();
	}
}
